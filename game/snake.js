/*
Create by Learn Web Developement
Youtube channel : https://www.youtube.com/channel/UC8n8ftV94ZU_DJLOLtrpORA
*/

const cvs = document.getElementById("snake");     // query
const ctx = cvs.getContext("2d");                 // query

// create the unit
const box = 32; // playable square units and grabable mutation item

// load images

const ground = new Image();
ground.src = "img/borderone.jpg";

const foodImg = new Image();
foodImg.src = "img/food.png";

const newHeadImg = new Image();
newHeadImg.src = "img/Taco.jpg";

// load audio files

let startup = new Audio();
let dead = new Audio();
let eat = new Audio();
let up = new Audio();
let right = new Audio();
let left = new Audio();
let down = new Audio();

startup.scr = "audio/billiondolar.wav"
dead.src = "audio/ibeatchina.mp3";
eat.src = "audio/mexico.wav";
up.src = "audio/up.mp3";
right.src = "audio/right.mp3";
left.src = "audio/left.mp3";
down.src = "audio/down.mp3";

// create the snake

let snake = []; // snake is an array of objects, the game mutates and drops them in here.

snake[0] = {    // spawn co-ordinate point of starting snake head
    x : 9 * box,
    y : 10 * box,
    imagefile: newHeadImg
};

// create the food

let food = {
    x : Math.floor(Math.random()*17+1) * box,  // floor rounds the random output down. defines the random spawn CoORD on playable area.
    y : Math.floor(Math.random()*15+3) * box
}

// create the score var

let score = 0;

//control the snake

let d; // head of the snake?? variable for control declared, rapid mutation variable



document.addEventListener("keydown",direction);         // query

function direction(event){                               //
    let key = event.keyCode;
    if( key == 37 && d != "RIGHT"){
        left.play();
        d = "LEFT";
    }else if(key == 38 && d != "DOWN"){
        d = "UP";
        up.play();
    }else if(key == 39 && d != "LEFT"){
        d = "RIGHT";
        right.play();
    }else if(key == 40 && d != "UP"){
        d = "DOWN";
        down.play();
    }
}

// check self collision function
function collision(head,array){
    for(let i = 0; i < array.length; i++){
        if(head.x == array[i].x && head.y == array[i].y){
            return true;
        }
    }
    return false;
}

// draw everything to the canvas

function draw(){
    
    ctx.drawImage(ground,0,0);


    for( let i = 0; i < snake.length ; i++){
        ctx.fillStyle = ( i == 0 )? newHeadImg : "yellow";

        ctx.fillRect(snake[i].x,snake[i].y,box,box);
        
        ctx.strokeStyle = "red";
        ctx.strokeRect(snake[i].x,snake[i].y,box,box);
    }
    
    ctx.drawImage(foodImg, food.x, food.y);
    
    // old head position
    let snakeX = snake[0].x;
    let snakeY = snake[0].y;
    
    // which direction
    if( d == "LEFT") snakeX -= box;
    if( d == "UP") snakeY -= box;
    if( d == "RIGHT") snakeX += box;
    if( d == "DOWN") snakeY += box;
    
    // if the snake eats the food
    if(snakeX == food.x && snakeY == food.y){
        score++;
        eat.play();                                 // score,sound,regenerate food
        food = {
            x : Math.floor(Math.random()*17+1) * box,
            y : Math.floor(Math.random()*15+3) * box
        }
        // we don't remove the tail
    }else{
        // remove the tail
        snake.pop();                        // as we move forward we erase the past
    }
    
    // add new Head
    
    let newHead = {
        x : snakeX,
        y : snakeY,
        imagefile : newHeadImg

    }
    
    // game over
    
    if(snakeX < 0 || snakeX > 18 * box || snakeY < 0 || snakeY > 18*box || collision(newHead,snake)){ //plugged into collision function
        clearInterval(game);
        dead.play();
    }

    document.addEventListener("keydown",refresh);       
    function refresh(event){                              
    let key = event.keyCode;
    if(key == 71) {
        startup.play();
    }
}
    
    snake.unshift(newHead);  // add newHead Co ordinates onto the front of the snake[] array
    
    ctx.fillStyle = "white";     // draw/update score board
    ctx.font = "45px Changa one";
    ctx.fillText(score,2*box,1.6*box);
}

// call draw function every 100 ms

let game = setInterval(draw,100);


















